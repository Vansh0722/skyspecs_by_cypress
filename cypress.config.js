const { defineConfig } = require("cypress");
{ chromeWebSecurity: false }

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      { chromeWebSecurity: false }
    },
  },
});
