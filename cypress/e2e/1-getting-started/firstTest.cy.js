describe("My First Test", () => {
  it("Verify title of page", () => {
    cy.visit("https://dev-platform.fincovi.com/#/");
    cy.title().should("eq", "Fincovi");
  });
  it("first tap", () => {
    cy.get(".btn").should("be.visible").click();
  });
  it("LogIn Details", () => {
    
    cy.visit(
      "https://fincovidev.b2clogin.com/fincovidev.onmicrosoft.com/b2c_1_sign_in_v2_sa/oauth2/v2.0/authorize?response_type=id_token&scope=openid%20profile&client_id=ddc12683-2efe-4430-9bcd-7e28fbcecb02&redirect_uri=https%3A%2F%2Fdev-platform.fincovi.com&state=eyJpZCI6ImY0ZmYwYjg4LThjNjAtNGViMS1hYmU3LTlmOWJjNTA2NzIwZCIsInRzIjoxNjYwOTA3ODcyLCJtZXRob2QiOiJyZWRpcmVjdEludGVyYWN0aW9uIn0%3D&nonce=590e4f87-d878-409f-b85d-21870684bf65&client_info=1&x-client-SKU=MSAL.JS&x-client-Ver=1.4.15&client-request-id=c74c0545-2812-4d91-adb1-6bbf5f1cae51&response_mode=fragment"
    );
    cy.title().should("eq", "Sign in");
    cy.get("#logonIdentifier")
      .should("be.visible")
      .type("vanshchaudhary0107@gmail.com");
    cy.get("#password").should("be.visible").type("skySpecs@22");
    cy.get("#next").should("be.visible").click();
   
  });
});
